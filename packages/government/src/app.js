"use strict";

const fs = require('fs');
const express = require('express');

const helmet = require('helmet');
const path = require('path');
const app = express();

// log all requests
app.use((req, res, next) => {
	console.log('['+ req.method + '] ' + req.url);
	return next();
});	

// connect static assets 
app.use(express.static(path.join(__dirname, 'static')));


// routes
app.get('/', (req, res) =>{
	return res.send('Overheid');	 
});

app.get('/config.kees.json', (req, res) =>{

	let json = fs.readFileSync(path.join(__dirname, 'config.kees.json'), 'utf8');
	
	json = json.replaceAll('[baseurl]', process.env.GOVERNMENT_URL);

	res.setHeader('Content-Type', 'application/json');
	return res.json(JSON.parse(json));	 
});

app.get('/config.marian.json', (req, res) =>{
	
	let json = fs.readFileSync(path.join(__dirname, 'config.marian.json'), 'utf8');
	
	json = json.replaceAll('[baseurl]', process.env.GOVERNMENT_URL);
	
	res.setHeader('Content-Type', 'application/json');
	return res.json(JSON.parse(json));	 	 
});


// handle errors
app.use(function (err, req, res, next) {
	
	if (res.headersSent) {
		return next(err);
	}
  
	return res.status(500).send(err.message);
});

	
module.exports = app;