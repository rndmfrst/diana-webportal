"use strict";

const app = require('schluss');
const government = require('government');
const insurer = require('insurer');
const notary = require('notary');
const bank = require('bank');


app.use('/' + process.env.GOVERNMENT_ENDPOINT, government);
app.use('/' + process.env.INSURER_ENDPOINT, insurer);
app.use('/' + process.env.NOTARY_ENDPOINT, notary);
app.use('/' + process.env.BANK_ENDPOINT, bank);


module.exports = app;